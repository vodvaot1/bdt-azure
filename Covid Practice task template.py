# Databricks notebook source
# MAGIC %md
# MAGIC ## Cvičení 
# MAGIC 
# MAGIC V notebooku se nachází jednoduché dílčí úkoly. Některé z nich jsou zakonenčeny asserty, pokud vám asserty nebudou procházet - pravděpodobně máte něco špatně nebo jste nepostupovali podle zadání. Pokud asserty prochází, neznamená to, že to máte nutně správně. Úkoly na sebe navazují, proto se ujistěte, že jste splnili zadání, než se posunete dál.
# MAGIC 
# MAGIC Notebook bude umět:
# MAGIC - Přijímat parametry, např. z Azure Data Factory.
# MAGIC - Načíst data o testech a populaci v ČR z Azure Blob Storage do pyspark DataFrame (pozor, není to samé jako pandas DataFrame!)
# MAGIC - S DataFrami dále pracovat, provést jednoduchou analýzu dat.
# MAGIC - Data vizualizovat.

# COMMAND ----------

# MAGIC %md 
# MAGIC ### Nastavení výchozích hodnot parametrů
# MAGIC 
# MAGIC #### Úkol :
# MAGIC - Výchozí hodnoty nastavte podle toho, jak jste si pojmenovali váš Storage Account, kontejnery
# MAGIC   - vyplňte váš *storage_account_access_key*
# MAGIC   - *directory* ideálně nastavte na nějaká data (zpracovaná vaší pipelinou) z minulosti, abyste mohli notebook spouštět i bez spuštění celé pipeline
# MAGIC     - pokud taková data nemáte, spusťte vaší pipeline - výstupem prvních dvou aktivit by měl být adresář ve vaší storage - jeho jméno použijte jako výchozí hodnotu
# MAGIC     - že data nemusí být aktuální nevadí, protože výchozí hodnotu budeme používat jen pro testovací účely

# COMMAND ----------

dbutils.widgets.removeAll()

# COMMAND ----------

dbutils.widgets.text("storage_account_name", "covidstorage123") # the name of your default storage account
dbutils.widgets.text("storage_account_access_key", "0YoXBxhmsHw5PfEbCOGC3ryAZATQdCRk4ZxMfmzJm1y/bEJoyqSdrNZWLOYsZc8x7TrFpQQH31mPsLBpJt+SoQ==")
dbutils.widgets.text("container_name", "cumulative-tests")

# if you already have some directory with joined covid data, you can set it as default value so you can test the notebook without running the pipeline
dbutils.widgets.text("directory", "covid_tests_joined-2021-12-01T17-57-13.6085986Z")


# COMMAND ----------

# MAGIC %md 
# MAGIC ##### Načtení parametrů z ADF (pokud jsou)
# MAGIC V případě, že notebook spouštíte manuálně nebo bez parametrů, použijí se výchozí hodnoty.

# COMMAND ----------

storage_account_name = dbutils.widgets.get("storage_account_name")
storage_account_access_key = dbutils.widgets.get("storage_account_access_key")
directory = dbutils.widgets.get("directory")
container_name = dbutils.widgets.get("container_name")

print(directory)

# COMMAND ----------

# MAGIC %md
# MAGIC ### Mounting kontejnerů
# MAGIC Abychom mohli pracovat se soubory co máme v Azure Blob Storage, namountujeme si danné kontejnery do DBFS.
# MAGIC Mount je pointer na kontejner. Pro připojení využijeme Access key, který předáme jako parametr z ADF. Není to nejbezpečnější možnost ale pro naše účely postačující, více informací o mountování Azure Storage např. [zde](https://docs.microsoft.com/en-us/azure/databricks/data/data-sources/azure/azure-storage).

# COMMAND ----------

# helper function which checks if a certain path is contained in current mounts, and if yes, unmounts it
def sub_unmount(str_path):
    if any(mount.mountPoint == str_path for mount in dbutils.fs.mounts()):
        dbutils.fs.unmount(str_path)

# COMMAND ----------

# mounts the countainer to specified mount_point, unless the mount_point already exists
def mount(source_url, mount_point, extra_configs):
    
    if not any(mount.mountPoint == mount_point for mount in dbutils.fs.mounts()):
        # mount point does not exist yet
       
        return dbutils.fs.mount(source = source_url,
                         mount_point = mount_point,
                         extra_configs = extra_configs)
     
    else:
        # do things you want to do if the mount point already exists
        return True

# COMMAND ----------

# MAGIC %md
# MAGIC ##### Sestavení parametrů a mount

# COMMAND ----------

# build the urls of our containers using a set of well-defined rules
source_url_tests = f"wasbs://{container_name}@{storage_account_name}.blob.core.windows.net/"

# where to mount the container
mount_point_tests = "/mnt/tests-data"

# access
extra_configs = {f"fs.azure.account.key.{storage_account_name}.blob.core.windows.net":storage_account_access_key}

# MOUNT IT!
assert mount(source_url_tests, mount_point_tests, extra_configs)

# COMMAND ----------

# MAGIC %md
# MAGIC #### Úkol :
# MAGIC - Stejným způsobem namountujte i kontejner obsahující data o populaci v krajích ČR. (Soubor byste měli mít stažený a umístěný v kontejneru spolu s Excel sheetem, který jsme používali v data flow.)

# COMMAND ----------

container_name_static_data = "static-data"

# ---------------------------------------
# WRITE YOUR CODE HERE:

source_url_static_data = f"wasbs://{container_name_static_data}@{storage_account_name}.blob.core.windows.net/"
mount_point_static_data = "/mnt/static-data"

# ---------------------------------------

# (extra_configs are the same as above)
mount(source_url_static_data, mount_point_static_data, extra_configs)

assert len(dbutils.fs.mounts()) > 1

# COMMAND ----------

# MAGIC %md
# MAGIC Po namountování kontejnerů můžeme použít DataBricks utility pro práci s filesystémem, abychom si vylistovali obsah namountovaného kontejneru a ověřili si, že se kontejner namountoval správně.
# MAGIC 
# MAGIC #### Úkol:
# MAGIC - Použijte příkaz **dbutils.fs.help()** pro zobrazení utilit pro práci s filesystémem.
# MAGIC - Pomocí vhodné utility vylistujte obsah alespoň jednoho z kontejnerů.

# COMMAND ----------

#dbutils.fs.help()
# ---------------------------------------
# WRITE YOUR CODE HERE:
dbutils.fs.ls(f"dbfs:/mnt/static-data")
# ---------------------------------------

# COMMAND ----------

# MAGIC %md
# MAGIC ## Načtení dat
# MAGIC 
# MAGIC Načteme data z konkrétního adresáře v našem kontejneru (využijeme k tomu vytvořený mount point) do DataFrame. Následně vytiskneme jeho schema.

# COMMAND ----------

# read the data about tests to dataframe
tests_df = spark.read\
    .option("header","true")\
    .option("inferSchema", "true")\
    .csv(f"{mount_point_tests}/{directory}")

# print the schema, check how the column types were infered
tests_df.printSchema() 

# check what type it is
print(type(tests_df))


# COMMAND ----------

# MAGIC %md
# MAGIC #### Úkol : 
# MAGIC - Načtěte data s údaji o populaci do dataframe, pojmenujte jej *population_df*.
# MAGIC - Dropněte sloupce *male_population*, *female_population*, *nazev_kraje* - nebudeme je potřebovat. [Můžete se inspirovat třeba zde.](https://towardsdatascience.com/delete-columns-pyspark-df-ba9272db1bb4)
# MAGIC - Zobrazte schema dataframu (měl by mít 2 sloupce - kód kraje a jeho populaci).

# COMMAND ----------

population_filename = "czech_population.csv"

# ---------------------------------------
# WRITE YOUR CODE HERE:

population_df = spark.read\
    .option("header","true")\
    .option("inferSchema", "true")\
    .csv(f"{mount_point_static_data}/{population_filename}")
    
population_df = population_df.select("CZ-NUTS3", "total_population")

# ---------------------------------------

df_shape = (population_df.count(), len(population_df.columns))
assert df_shape == (14, 2)

# COMMAND ----------

# MAGIC %md
# MAGIC #### Funkce display
# MAGIC 
# MAGIC Databricks nabízí vestavěnou funkci **display**. Slouží k hezké vizualizaci dat - pomocí tabulek či nejrůznějších grafů. [Více o vizualizacích pomocí display si můžete přečíst zde.](https://docs.databricks.com/notebooks/visualizations/index.html)

# COMMAND ----------

display(tests_df)

# COMMAND ----------

display(population_df)

# COMMAND ----------

# MAGIC %md
# MAGIC #### Úkol :
# MAGIC - Vytvořte DataFrame se jménem *total_tests_per_district_df*, který bude obsahovat 3 sloupce:
# MAGIC   - název kraje
# MAGIC   - jeho CZ-NUTS3 kód
# MAGIC   - kolik bylo v kraji celkem provedeno testů (nejaktuálnější údaj)
# MAGIC     - můžete např. vzít maximum ze sloupce počtu kumulativních testů pro každý kraj
# MAGIC - Sloupec s testy pojmenujte *total_tests*.
# MAGIC - Seřaďte záznamy v DataFramu sestupně podle počtu provedených testů.
# MAGIC - Zobrazte výsledný DataFrame funkcí display.
# MAGIC - Zkontrolujte, jestli prochází asserty.
# MAGIC 
# MAGIC Užitečné funkce: [groupBy](https://spark.apache.org/docs/latest/api/python/reference/api/pyspark.sql.DataFrame.groupBy.html), max, withColumnRenamed, orderBy/sort,
# MAGIC   

# COMMAND ----------

from pyspark.sql.functions import col

# ---------------------------------------
# WRITE YOUR CODE HERE:

total_tests_per_district_df = tests_df.withColumnRenamed("CZ-NUTS3", "CZ-NUTS3-TEST")
total_tests_per_district_df = total_tests_per_district_df.join(population_df, total_tests_per_district_df["CZ-NUTS3-TEST"] == population_df["CZ-NUTS3"],"inner")\
        .groupby("nazev_kraje", "CZ-NUTS3")\
        .agg({'kumulativni_pocet_testu_kraj': 'max'})\
        .withColumnRenamed("max(kumulativni_pocet_testu_kraj)", "total_tests")\
        .orderBy(col("total_tests").desc())
    
display(total_tests_per_district_df)

# ---------------------------------------

df_shape = (total_tests_per_district_df.count(), len(total_tests_per_district_df.columns))
assert df_shape == (14, 3)
assert total_tests_per_district_df.collect()[0].total_tests > total_tests_per_district_df.collect()[13].total_tests

# COMMAND ----------

# MAGIC %md
# MAGIC #### Přidání sloupce test_ratio :
# MAGIC - Do dataframu s počtem testů dle krajů (*total_tests_per_district_df*) přidáme sloupec *test_ratio*, který bude obsahovat číslo spočtené vzorcem: (testů v kraji)/(testů celkem).
# MAGIC   - Zaokrouhlíme na 4 desetinná místa.
# MAGIC 
# MAGIC Užitečné funkce: withColumn, [col](https://spark.apache.org/docs/latest/api/python/reference/api/pyspark.sql.functions.col.html)

# COMMAND ----------

import pyspark.sql.functions as F 

total_tests_column_name = "total_tests"

# calculate the total number of tests and store it as integer
total_test_amount = total_tests_per_district_df.agg(F.sum(total_tests_column_name)).collect()[0][0]

# calculate the ratio of (tests in district)/(tests total)
total_tests_per_district_df = total_tests_per_district_df.withColumn("test_ratio", F.round(col(total_tests_column_name)/total_test_amount,4))

print(total_test_amount)
ratio_sum = total_tests_per_district_df.agg(F.sum("test_ratio")).collect()[0][0]
assert abs(1 - ratio_sum) <= 0.01
collected = total_tests_per_district_df.collect()
assert collected[0].test_ratio > collected[13].test_ratio


for i in range(14):
    assert abs((1/collected[i].test_ratio) * collected[i].total_tests - total_test_amount) <= 5e4

display(total_tests_per_district_df)

# COMMAND ----------

# MAGIC %md
# MAGIC #### Úkol :
# MAGIC - Do vašeho DataFramu, který obsahuje data o populaci jednotlivých krajů (*population_df*) přidejte sloupec *population_ratio*, který bude vyjadřovat poměr (lidí v kraji)/(lidí celkem).
# MAGIC   - pomocí funkce round (pyspark.sql.functions.round) hodnoty zakrouhlete na 4 desetinná místa 
# MAGIC - Můžete postupovat obdobně jako při situaci s testy.
# MAGIC - Zkontrolujte, že prochází asserty.

# COMMAND ----------

# ---------------------------------------
# WRITE YOUR CODE HERE:

total_population = population_df.agg(F.sum("total_population")).collect()[0][0]
print("Total population in CR: " + str(total_population))

population_df = population_df.withColumn("population_ratio", F.round(col("total_population")/total_population,4))
# ---------------------------------------

collected = population_df.collect()

for i in range(0,14):
    assert abs((1/collected[i].population_ratio) * collected[i].total_population - 1e7) <= 2*1e6
display(population_df)

# COMMAND ----------

# MAGIC %md
# MAGIC #### Úkol :
# MAGIC - Pokud se sloupec s **NUTS-3** kódem v obou dataframech *total_tests_per_district_df* a *population_df* jmenuje stejně, jeden z nich přejmenujte.
# MAGIC - Vytvořte nový dataframe *tests_population_df*, který bude spojením *total_tests_per_district_df* a *population_df* na základě kódu kraje.
# MAGIC - Z výsledného dataframu dropněte jeden sloupec s NUTS-3 kódem (za předpokladu, že vám po joinu vznikla tabulka obsahující sloupce s obou zdrojových dataframů).
# MAGIC - Přidejte do dataframu sloupec *similar_ratios*, jehož hodnota bude buď:
# MAGIC   - **True** v případě, že hodnoty *population_ratio* a *test_ratio* se o sebe liší o méně nebo rovno *threshold*
# MAGIC   - **False** liší li se hodnoty o více než je *threshold*
# MAGIC - Význam sloupce *similar_ratios* je odpověď na otázku: Odpovídá zhruba poměr testů v kraji jeho poměrné populaci?
# MAGIC - Zkontrolujte, zda prochází asserty.
# MAGIC 
# MAGIC Užitečné funkce: join, drop, withColumn, funkce z pyspark.sql.functions jako [when+otherwise](https://spark.apache.org/docs/latest//api/python/reference/api/pyspark.sql.functions.when.html), abs, col

# COMMAND ----------

import pyspark.sql.functions as F

threshold = 0.01
# ---------------------------------------
# WRITE YOUR CODE HERE:

population_df = population_df.withColumnRenamed("CZ-NUTS3", "CZ-NUTS3-2")

tests_population_df = population_df.join(total_tests_per_district_df, population_df["CZ-NUTS3-2"] == total_tests_per_district_df["CZ-NUTS3"], "inner").drop("CZ-NUTS3-2")

tests_population_df = tests_population_df.withColumn("similar_ratios", F.when(F.abs(col("population_ratio") - col("test_ratio")) <= threshold, "True").otherwise("False"))
# ---------------------------------------

collected = tests_population_df.collect()

for i in range(14):
    if abs(collected[i].population_ratio-collected[i].test_ratio) <= threshold:
        assert collected[i].similar_ratios in (True, "True")
    else:
        assert collected[i].similar_ratios in (False, "False")

display(tests_population_df)


# COMMAND ----------

# MAGIC %md
# MAGIC #### Úkol :
# MAGIC 
# MAGIC - Udělejte nějakou hezkou vizualizaci dle vlastního uvážení. Jakou vizualizaci a jakým způsobem uděláte je na vás.
# MAGIC - Inspirovat se můžete koláči níže.

# COMMAND ----------

import matplotlib.pyplot as plt
import numpy as np

fig, ax = plt.subplots(1,1,figsize=(30,10))

counties = tests_population_df.select("nazev_kraje").rdd.map(lambda r: r[0]).collect()
total_tests_per_county = tests_population_df.select("total_tests").rdd.map(lambda r: r[0]).collect()
total_population_per_county = tests_population_df.select("total_population").rdd.map(lambda r: r[0]).collect()

x = np.arange(len(counties))
width = 0.4

ax.bar(x - width/2, total_tests_per_county, width, color = 'darkorange', label="Počet testů")
ax.bar(x + width/2, total_population_per_county, width, color = 'lightblue', label="Počet obyvatel")

ax.set_title('Poměr počtu obyvatel s celkovým počzem provedených testů dle jednotlivých krajů')
ax.set_xticks(x)
ax.set_xticklabels(counties)
ax.legend()

plt.show()

# COMMAND ----------

# MAGIC %md
# MAGIC Koláčové grafy vizualizující počty populace/testů dle jednotlivých krajů.

# COMMAND ----------

# The Databricks Runtime includes the seaborn visualization library.
import seaborn as sns
import matplotlib.pyplot as plt

fig, ax = plt.subplots(1,2,figsize=(20,10))

data_pop = tests_population_df.select("total_population").rdd.map(lambda r: r[0]).collect()
data_test = tests_population_df.select("total_tests").rdd.map(lambda r: r[0]).collect()

labels = tests_population_df.select("nazev_kraje").rdd.map(lambda r: r[0]).collect()

#define Seaborn color palette to use
colors = sns.color_palette('pastel')

#create pie chart
ax[0].pie(data_pop, labels = labels, colors = colors, autopct='%.0f%%')
ax[0].set_title("Population share")

ax[1].pie(data_test, labels = labels, colors = colors, autopct='%.0f%%')
ax[1].set_title("Covid tests share")
fig.show()

# COMMAND ----------


